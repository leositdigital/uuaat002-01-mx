package com.bbva.uuaa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Consulta importe cuenta bancaria
 *
 */
public class UUAAT00201MXTransaction extends AbstractUUAAT00201MXTransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(UUAAT00201MXTransaction.class);

	/**
	 * The execute method...
	 */
	@Override
	public void execute() {
		// TODO - Implementation of business logic
	}

}
